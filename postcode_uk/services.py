r"""Support for validation postcode.

This module provides one class that verify that postcode is standard
UK and that format postcode.

Stand Format:
Format   	|Coverage	                                |Example  |
------------------------------------------------------------------|
AA9A 9AA	|WC postcode area; EC1–EC4, NW1W, SE1P, SW1	|EC1A 1BB | 
------------------------------------------------------------------|
A9A 9AA 	|E1W, N1C, N1P	                            |W1A 0AX  |
------------------------------------------------------------------|
A9 9AA  	|B, E, G, L, M, N, S, W                 	|M1 1AE   |
A99 9AA	    |                                           |B33 8TH  |
------------------------------------------------------------------|
AA9 9AA 	|All other postcodes	                    |CR2 6XH  |
AA99 9AA	|                                           |DN55 1PT |
------------------------------------------------------------------|

"""  # noqa

import re


class PostcodeErrorUK(Exception):
    """
    Handle error in Postcode class
    """


class PostcodeUK:
    def __init__(self, postcode: str):
        self.postcode = postcode

    @property
    def is_valid(self):
        """
        Verify if it's a postcode uk

        :param postcodes: postcode that should to be validate.
        :return: boolean, if it's correct postcode return True, if not return False
        """

        postcodes_regex = '^(([A-Z]{1,2}[0-9][A-Z0-9]?|ASCN|STHL|TDCU|BBND|[BFS]IQQ|PCRN|TKCA) ?[0-9][A-Z]{2}|BFPO ?' \
                          '[0-9]{1,4}|(KY[0-9]|MSR|VG|AI)[ -]?[0-9]{4}|[A-Z]{2} ?[0-9]{2}|GE ?CX|GIR ?0A{2}|SAN ?TA1)$'

        is_validate = bool(re.search(postcodes_regex, self.postcode))
        return is_validate

    @property
    def formatted(self):
        """
        Function to format the post code and return the formatted value with inward and outward code

        :param postcodes: postcode that should to be validate.
        :return: postcode formatted
        """
        # Replace all the space from the string
        postcode_str = self.postcode.replace(" ", "")

        # Check the length of input postcode and return error message if len of postcode(with space) is greater than 7
        # chars and less than 5 chars
        if len(postcode_str) < 5 or len(postcode_str) > 7:
            message = "ERROR: 5 to 8 characters only"
            raise PostcodeErrorUK(message)

        # Throw error message if the input postcode contains special symbols
        if not postcode_str.isalnum():
            message = "ERROR: No special Characters allowed"
            raise PostcodeErrorUK(message)

        # Converting the post code to upper case
        postcode_str = postcode_str.upper()
        # Extracting out the outward code from input postcode string as all chars except last 3
        outward_code = postcode_str[:-3]
        # Extracting out the inward code as last 3 chars from input postcode string
        inward_code = postcode_str[-3:]
        # Joining the outward and inward code separated by space to form formatted postcode
        formatted_postcode = outward_code + " " + inward_code

        return formatted_postcode
