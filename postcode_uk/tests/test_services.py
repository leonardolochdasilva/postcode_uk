import pytest

from postcode_uk.services import PostcodeUK, PostcodeErrorUK


def test_should_check_postcode_is_validate():
    # GIVEN
    postcode = "BS98 1TL"

    # WHEN
    result = PostcodeUK(postcode).is_valid

    # THEN
    assert result is True


def test_should_not_check_postcode_is_validate_due_postcode_outline():
    # GIVEN
    postcode = "ECAA 1BB"

    # WHEN
    result = PostcodeUK(postcode).is_valid

    # THEN
    assert result is False


def test_should_format_postcode():
    # GIVEN
    postcode = "BS981TL"

    # WHEN
    result = PostcodeUK(postcode).formatted

    # THEN
    assert result == "BS98 1TL"


def test_should_not_format_postcode_due_there_is_not_minimal_code():
    # GIVEN
    postcode = "BS98"

    # WHEN
    with pytest.raises(PostcodeErrorUK) as e:
        PostcodeUK(postcode).formatted

    # THEN
    assert e.value.args[0] == 'ERROR: 5 to 8 characters only'


def test_should_not_format_postcode_due_there_is_special_characters():
    # GIVEN
    postcode = "BS981T$"

    # WHEN
    with pytest.raises(PostcodeErrorUK) as e:
        PostcodeUK(postcode).formatted

    # THEN
    assert e.value.args[0] == 'ERROR: No special Characters allowed'
