# Postcode UK

Library to verify if postcode is from UK.


### Virtual Environment

To create a virtual environment, simply type in the following command:

```sh
$ make create-env
```

### Installation

Install the lib.

```sh
$ pip install git+https://gitlab.com/leonardolochdasilva/postcode_uk.git
```

### Tests
For run test
```sh
$ make
```

In this project, there is a pipeline, on Gitlab, that executes the test convention and code to verify
that the change is correct and follows the pattern.
If no pipeline is passed, a change will not be allowed to merge with the master